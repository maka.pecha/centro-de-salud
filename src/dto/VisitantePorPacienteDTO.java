/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Maka
 */
public class VisitantePorPacienteDTO {
    private String paciente;
    private int cantidad;

    public VisitantePorPacienteDTO(String paciente, int cantidad) {
        this.paciente = paciente;
        this.cantidad = cantidad;
    }

    public String getPaciente() {
        return paciente;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "VisitantePorPaciente{" + "paciente=" + paciente + ", cantidad=" + cantidad + '}';
    }
    
}
