/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import modelo.Empleado;
import modelo.Paciente;

/**
 *
 * @author Maka
 */
public class VisitaDTO {
    private int idVisita;
    private String paciente;
    private String empleado;
    private String nombre;
    private int duracion;

    public VisitaDTO(int idVisita, String paciente, String empleado, String nombre, int duracion) {
        this.idVisita = idVisita;
        this.paciente = paciente;
        this.empleado = empleado;
        this.nombre = nombre;
        this.duracion = duracion;
    }

    public int getIdVisita() {
        return idVisita;
    }

    public String getPaciente() {
        return paciente;
    }

    public String getEmpleado() {
        return empleado;
    }

    public String getNombre() {
        return nombre;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setIdVisita(int idVisita) {
        this.idVisita = idVisita;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    @Override
    public String toString() {
        return "VisitaDTO{" + "idVisita=" + idVisita + ", paciente=" + paciente + ", empleado=" + empleado + ", nombre=" + nombre + ", duracion=" + duracion + '}';
    }
    
}
