/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Maka
 */
public class Visita {
    private int idVisita;
    private Paciente paciente;
    private Empleado empleado;
    private String nombre;
    private int duracion;

    public Visita(int idVisita, Paciente paciente, Empleado empleado, String nombre, int duracion) {
        this.idVisita = idVisita;
        this.paciente = paciente;
        this.empleado = empleado;
        this.nombre = nombre;
        this.duracion = duracion;
    }

    public int getIdVisita() {
        return idVisita;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public String getNombre() {
        return nombre;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setIdVisita(int idVisita) {
        this.idVisita = idVisita;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    @Override
    public String toString() {
        return "Visita{" + "idVisita=" + idVisita + ", paciente=" + paciente + ", empleado=" + empleado + ", nombre=" + nombre + ", duracion=" + duracion + '}';
    }
    
    
}
