/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestor;

import java.sql.*;
import java.util.ArrayList;
import modelo.*;
import dto.*;

/**
 *
 * @author Maka
 */
public class gestorDB {
    private Connection con;
    private String CONN = "jdbc:sqlserver://localhost\\SQLEXPRESS:1434;databaseName=Visitas";
    private String USER= "sa";
    private String PASS= "38161112";
    
    public void abrirConexion() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(CONN, USER, PASS);
        } catch (Exception exc){
            exc.printStackTrace();
        }
    }
    
    public void cerrarConexion(){
        try {
            if(con != null && !con.isClosed()){
                con.close();
            }
        } catch (Exception exc){
            exc.printStackTrace();
        }
    }
    
    public ArrayList<Paciente> obtenerPacientes(){
        ArrayList<Paciente> lista = new ArrayList<>();
        
        try {
            abrirConexion();
            
            Statement st = con.createStatement();
            String query = "SELECT * FROM Pacientes";
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next()){
                int IdPaciente = rs.getInt("IdPaciente");
                String Nombre = rs.getString("Nombre");
                Paciente p = new Paciente(IdPaciente, Nombre);
                lista.add(p);
            }
        
        } catch(Exception exc) {
            exc.printStackTrace();
        } finally {
            cerrarConexion();
        }
        return lista;
    }
    
    public ArrayList<Empleado> obtenerEmpleados(){
        ArrayList<Empleado> lista = new ArrayList<>();
        
        try {
            abrirConexion();
            
            Statement st = con.createStatement();
            String query = "SELECT * FROM Empleados";
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next()){
                int Legajo = rs.getInt("Legajo");
                String Nombre = rs.getString("Nombre");
                Empleado e = new Empleado(Legajo, Nombre);
                lista.add(e);
            }
        
        } catch(Exception exc) {
            exc.printStackTrace();
        } finally {
            cerrarConexion();
        }
        return lista;
    }
    
     public void agregarVisita(Visita v){
        try{
            abrirConexion();
            
            String query = "INSERT INTO Visitas VALUES (?,?,?,?)";
            PreparedStatement ps = con.prepareCall(query);
            ps.setInt(1, v.getPaciente().getIdPaciente());
            ps.setInt(2, v.getEmpleado().getLegajo());
            ps.setString(3, v.getNombre());
            ps.setInt(4, v.getDuracion());
            ps.executeUpdate();
            ps.close();
            
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally
        {
            cerrarConexion();
        }
    }
    
    public ArrayList<VisitaDTO> obtenerVisitasDTO(){
        ArrayList<VisitaDTO> lista = new ArrayList<>();
        
        try {
            abrirConexion();
            
            Statement st = con.createStatement();
            String query = "select v.IdVisita, p.Nombre as paciente, e.Nombre as empleado, v.Nombre as nombreVisita, v.Duracion\n" +
                            "from Visitas v\n" +
                            "join Pacientes p on v.IdPaciente = p.IdPaciente\n" +
                            "join Empleados e on v.LegajoRecepcionista = e.Legajo\n" +
                            "order by p.Nombre";
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next()){
                int id = rs.getInt("idVisita");
                String paciente = rs.getString("paciente");
                String empleado = rs.getString("empleado");
                String nombreVisita = rs.getString("nombreVisita");
                int duracion = rs.getInt("Duracion");
                
                VisitaDTO o = new VisitaDTO(id, paciente, empleado, nombreVisita, duracion);
                lista.add(o);
            }
            rs.close();
            st.close();
            
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            cerrarConexion();
        }
        return lista;
    }
    
     public ArrayList<VisitantePorPacienteDTO> obtenerCantidadVisitasPorPaciente(){
        ArrayList<VisitantePorPacienteDTO> lista = new ArrayList<>();
        
        try {
            abrirConexion();
            
            Statement st = con.createStatement();
            String query = "select p.Nombre, count(*) as cantidad\n" +
                            "from Visitas v\n" +
                            "join Pacientes p on v.IdPaciente = p.IdPaciente\n" +
                            "group by  p.Nombre ";
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next()){
                String nombre = rs.getString("Nombre");
                int cantidad = rs.getInt("cantidad");
                
                VisitantePorPacienteDTO t = new VisitantePorPacienteDTO(nombre, cantidad);
                lista.add(t);
            }
            rs.close();
            st.close();
            
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            cerrarConexion();
        }
        return lista;
    }
    
    public double promedioVisitasMas10Minutos(){
        double promedio = 0;
        
        try {
            abrirConexion();
            
            Statement st = con.createStatement();
            ResultSet rs =st.executeQuery("select AVG(v.Duracion) from Visitas v where v.Duracion > 10");
            if(rs.next()) {
                promedio = rs.getDouble(1);
            }
            rs.close();
            st.close();
            } 
        catch (Exception exc) {
                exc.printStackTrace();
            }
        finally{
                cerrarConexion();
            }
        return promedio;
    }
}
